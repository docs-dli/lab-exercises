---
title: CI Pipeline Basics
slug: ci-pipeline-basics
version: 13.9
difficulty: easy
time_to_complete: 20-40
runner_required: true
cluster_required: false
---

# CI Pipeline Basics

## Overview

In this lab exercise, we are going to create a new `.gitlab-ci.yml` file, create a basic pipeline with a few jobs, and echo out console output to show how a pipeline works before adding any backend business logic or other executable actions.

* **Difficulty Level:** Easy
* **Estimated Time to Complete:** 20-40 mins
* **GitLab Version Tested:** 13.9
* **GitLab Runner Required:** Yes (Shared OK)
* **Kubernetes Cluster Required:** No

## Step-by-Step Instructions

**Learning styles and neurodiversity:** We want to help everyone be successful when learning how to use GitLab. We recognize that everyone learns differently. The step-by-step instructions will be helpful if you are not sure how to create a file, what a stage is, how to add a job, etc. If you have experience with using GitLab CI files or prefer to learn through high-level objectives instead of step-by-step instructions, you can scroll down to the [objective-based instructions](#objective-based-instructions) below.

1. On the GitLab project overview or repository page, **click the plus icon** below the project title and select **New File**.
    <kbd>![Screenshot](screenshots/ci-pipeline-basics-task1.png)</kbd>

2. In the **file name** field, type in `.gitlab-ci-yml`.
    <kbd>![Screenshot](screenshots/ci-pipeline-basics-task2.png)</kbd>

3. Copy and paste the following code to the top of the file to create the `build`, `test`, and `deploy` stages of your pipeline.
	```yaml
	# .gitlab-ci.yml

	stages:
      - build
      - test
      - deploy
    ```

4. Create a new job named `build` by adding the following code to the bottom of the `.gitlab-ci.yml` file. Be sure to add an extra line of space below the `stages` block. Note that each job must include a stage (that it is associated with) and a script (with actions to perform).
	```yaml
	# .gitlab-ci.yml

	stages:
	  - build
	  - test
      - deploy

	build:
	  stage: build
	  script: echo "Testing code..."
	```

5. Create a new job named `test A` by adding the following code to the bottom of the `.gitlab-ci.yml` file.
	```yaml
	build:
	  stage: build
	  script: echo "Testing code..."

	test A:
	  stage: test
	  script: echo "Testing code..."
	```

6. Create a new job named `test B` by adding the following code to the bottom of the `.gitlab-ci.yml` file.
    > The second test will show how multiple jobs can appear in a single stage.
	```yaml
	test A:
	  stage: test
	  script: echo "Testing code..."

	test B:
	  stage: test
	  script: echo "Testing code..."
	```

7. Create a new job named `deploy` by adding the following code to the bottom of the `.gitlab-ci.yml` file.
    > We are setting the job to `manual` to allow us to click a button to manually start the job instead of running it automatically with the pipeline.
	```yaml
	test B:
	  stage: test
	  script: echo "Testing code..."

	deploy:
	  stage: deploy
	  script: echo "Testing code..."
	  when: manual
	```

8. Verify that your file now looks like the following:
	```yaml
	# .gitlab-ci.yml

	stages:
	  - build
	  - test
	  - deploy

	build:
	  stage: build
	  script: echo "Testing code..."

	test A:
	  stage: test
	  script: echo "Testing code..."

	test B:
	  stage: test
	  script: echo "Testing code..."

	deploy:
	  stage: deploy
	  script: echo "Deplying code..."
	  when: manual
	```

9. At the bottom of the page,locate the **Commit message** form field and replace `Add new file` with `Creating .gitlab-ci.yml with custom pipeline`.

10. In the **Target Branch** field, replace `master` with `add-custom-ci-file` to create a new branch.
    > After you type in a different branch name, a new checkbox will appear to ask if you want to start a new merge request.

11. Ensure that the `Start a new merge request with these changes` is selected.
    <kbd>![Screenshot](screenshots/ci-pipeline-basics-task11.png)</kbd>

12. Click the green **Commit changes** button.

13. After the file has been created in the branch, you will be redirected to the `New Merge Request` page.
	<kbd>![Screenshot](screenshots/ci-pipeline-basics-task13.png)</kbd>

14. You can leave all of the fields at their default value. Scroll down and click the green **Submit merge request** button.
	<kbd>![Screenshot](screenshots/ci-pipeline-basics-task14.png)</kbd>

15. After the merge has been created, you will be able to see the pipeline associated with the merge request. **Click on the pipeline ID** to view the pipeline. You can also see your pipelines by navigating to **CI/CD > Pipelines** in the left navigation menu.
    <kbd>![Screenshot](screenshots/ci-pipeline-basics-task15.png)</kbd>

16. You should now have a basic understanding of how a `.gitlab-ci.yml` file is created and what stages and jobs are. In the future, you can follow the [Quick Start Instructions](#quick-start-instructions) if you don't need the step-by-step instructions.

17. Scroll down to the [learning objectives review](#learning-objectives-review) section below and follow the instructions.

## Quick Start Instructions

These instructions are for users who are familiar with creating CI files and understand how to add stages and jobs to your `.gitlab-ci.yml` files.

Please scroll up to the [step-by-step instructions](#step-by-step-instructions) section above if you are not familiar with these, or prefer step-by-step instructions and additional narrative of the configuration steps that you're performing.

1. In your GitLab project, create a new file named `.gitlab-ci.yml` with the following contents:
    > See steps 1-8 above for step-by-step instructions.
    ```yaml
    # .gitlab-ci.yml

    stages:
      - build
      - test
      - deploy

    build:
      stage: build
      script: echo "Testing code..."

    test A:
      stage: test
      script: echo "Testing code..."

    test B:
      stage: test
      script: echo "Testing code..."

    deploy:
      stage: deploy
      script: echo "Deplying code..."
      when: manual
    ```

2. Commit the file with the commit message of `Creating .gitlab-ci.yml with custom pipeline` to a new branch named `add-custom-ci-file` and start a new merge request.
    > See steps 9-12 above for step-by-step instructions.

3. Create the merge request and navigate to the pipeline that was started by your commit.
    > See steps 13-15 above for step-by-step instructions.

4. Scroll down to the [learning objectives review](#learning-objectives-review) section below and follow the instructions.

## Learning Objectives Review

Follow the instructions below, regardless of whether you used the [step-by-step instructions](#step-by-step-instructions) or [quick start instructions](#quick-start-instructions).

1. On the pipeline page, you will see a graph of the stages (each column) and each job (the rounded buttons).
    > The icon on the left side of the button will show the status of the job.

2. Click on the `test A` button to view details about the job.
    <kbd>![Screenshot](screenshots/ci-pipeline-basics-task17.png)</kbd>

3. If this is your first time seeing a pipeline job, we encourage you to take a few moments to familiarize yourself with the details about the job and what you can find in the console output.
    <kbd>![Screenshot](screenshots/ci-pipeline-basics-task18.png)</kbd>
    * **Red arrow** - You can see how long the job took to run. This will vary from a few seconds up to several minutes depending on the job. Many jobs in a more complex pipeline will take 1-3 minutes to complete.
    * **Orange arrow** - You can see the code commit that this pipeline is using to run. You can click on the link to view the source code if your job shows an error that you need to debug.
    * **Yellow arrow** - This is the pipeline ID that this job is associated with. You can click on the link to view the pipeline overview that you saw earlier.
    * **Green arrow** - This is the repository branch that this pipeline is running on, in conjunction with the commit ID. This is important since a job can be associated a merge request for a branch that is in development and a failed job may be expected, whereas a failed job on the `master` or `main` branch would mean that something has gone wrong.
    * **Blue arrow** - This is a drop down list of all of the stages in this pipeline. The list of jobs below are for the selected stage. You can switch to a different stage to see the different results.
    * **Purple arrow** - You can quickly switch to view other jobs in this stage.
    * **Red box** - This will show you what version of the GitLab Runner that you are using, and the runner instance that this job is being run on. This is helpful when debugging if there is a problem with a runner, or which infrastructure environment (ex. Kubernetes cluster) that your project is using for running CI jobs.
    * **Orange box** - This will show you the path to the images that are used. In many cases these will be images that GitLab publishes and are reusable across a wide range of projects.
    * **Yellow box** - This will show the output of any commands that you have added to the `script` for the job.
    * **Purple box** - This will show whether the job succeeded or failed. If a job has failed, the reason will usually be shown in the last 10-15 lines of the job console output.

You have successfully created a custom pipeline for this GitLab project. As you use more complex CI jobs, you should be able to locate information on the pipeline overview and job details page to help with debugging.

## Additional Reading

You can learn more about pipelines and jobs in the GitLab documentation.
* [Pipeline Documentation](https://docs.gitlab.com/ee/ci/pipelines/)
* [Jobs Documentation](https://docs.gitlab.com/ee/ci/jobs/)

As you continue learning about and using GitLab CI, you may find the following documentation pages helpful.

* [Continuous Integration (CI) Feature Overview](https://about.gitlab.com/product/continuous-integration/)
* [CI/CD Feature Docs](https://docs.gitlab.com/ee/ci/)
* [CI/CD Concepts Docs](https://docs.gitlab.com/ee/ci/introduction/)
* [CI/CD Pipeline Docs](https://docs.gitlab.com/ee/ci/pipelines/)
* [CI/CD Variables Docs](https://docs.gitlab.com/ee/ci/variables/README.html)
* [CI/CD Environments and Deployments Docs](https://docs.gitlab.com/ee/ci/environments/)
* [CI/CD with Runners Docs](https://docs.gitlab.com/ee/ci/runners/README.html)
* [Auto DevOps Docs](https://docs.gitlab.com/ee/topics/autodevops/)
* [CI/CD configuration with .gitlab-ci.yml Docs](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)
* [CI/CD authoring YAML reference for .gitlab-ci.yml Docs](https://docs.gitlab.com/ee/ci/yaml/README.html)
* [CI/CD Implementation Examples Docs](https://docs.gitlab.com/ee/ci/examples/README.html)
* [CI/CD Troubleshooting Docs](https://docs.gitlab.com/ee/ci/troubleshooting.html)
* [Migrate from CircleCI Docs](https://docs.gitlab.com/ee/ci/migration/circleci.html)
* [Migrate from Jenkins Docs](https://docs.gitlab.com/ee/ci/migration/jenkins.html)
